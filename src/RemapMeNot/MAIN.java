package RemapMeNot;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class MAIN {

	public static final String languageCode = "1033";
	public static final String locFrom = "Z:/windows";
	public static final String locTo = "D:/windows";

	private static final boolean debug = false;

	public static void main(String[] args) {
		setLNF();
		extractRH("ResHacker.exe");
		extractRH("ResHacker2.exe");
		String locFr = locFrom;
		String locT = locTo;
		String lgCode = languageCode;
		if (debug) {
			Window w = new Window(locFr, locT, lgCode);
			w.start();
		} else {
			new LocSet(locFr, locT, lgCode);
		}
	}

	private static void extractRH(String res) {
		InputStream rh = MAIN.class.getResourceAsStream("/RemapMeNot/"+res);
		File f = new File(res);
		if (f.exists()) {
			f.delete();
		}
		try {
			Files.copy(rh, f.toPath());
		} catch (IOException e1) {
			e1.printStackTrace();
			JOptionPane.showMessageDialog(null, "Failed to extract resource");
		}
	}

	private static void setLNF() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			return;
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e1) {
			e1.printStackTrace();
		}
		try {
			UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
	}

}
