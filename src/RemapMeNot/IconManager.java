package RemapMeNot;

import javax.swing.JPanel;

import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.JScrollPane;

import java.awt.FlowLayout;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.JComboBox;
import javax.swing.SwingUtilities;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class IconManager extends JPanel {

	private JComboBox<String> comboBox;
	private String root;
	private JPanel realContent;

	private static int instances = 0;

	private final File tempDir;
	private boolean started = false;
	private final File rh = new File("ResHacker.exe");
	private final File rh_cp;
	private final File target;

	private Map<String, ResIcon> mapping = new HashMap<String, ResIcon>();
	private JLabel lblNewLabel_1;

	public void start() {
		started = true;
	}

	private ResIcon selected;
	private String originalRoot;
	private boolean showReplaced;
	private ReplacerDB replacer;
	private List<String> mapar = new ArrayList<>();
	private KeyListener keyAdapt;

	public void setSelectedIcon(ResIcon i) {
		if (selected != null) {
			if (selected != i) {
				selected.setSelected(false);
				i.setSelected(true);
				selected = i;
			}
		} else {
			i.setSelected(true);
			selected = i;
		}
	}

	public String getRoot() {
		return root;
	}

	public IconManager(String root, boolean showReplaced, ReplacerDB replacer, KeyListener keyAdapt) {
		this.keyAdapt = keyAdapt;
		this.replacer = replacer;
		this.addKeyListener(keyAdapt);
		this.showReplaced = showReplaced;
		tempDir = new File("tmp_" + instances);
		rh_cp = new File(tempDir.getPath() + "/ResHacker.exe");
		target = new File(tempDir.getPath() + "/target.dll");
		instances++;
		this.root = root;
		setLayout(new BorderLayout(0, 0));

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.getHorizontalScrollBar().setUnitIncrement(16);
		scrollPane.getVerticalScrollBar().setUnitIncrement(16);
		add(scrollPane, BorderLayout.CENTER);

		realContent = new JPanel();
		scrollPane.setViewportView(realContent);
		realContent.setLayout(new WrapLayout(FlowLayout.CENTER, 5, 5));

		JPanel panel = new JPanel();
		add(panel, BorderLayout.NORTH);
		panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		JLabel lblArchive = new JLabel("Archive:");
		lblArchive.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(lblArchive);

		comboBox = new JComboBox<String>();
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					updateIcons();
				} catch (Exception e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(null, "Load icons failed");
				}
			}
		});
		comboBox.setMaximumRowCount(64);
		for (String location : Locations.Locations) {
			String file = root + "/" + location;
			File f = new File(file);
			if (f.exists()) {
				comboBox.addItem(location);
			}
		}
		panel.add(comboBox);

		JLabel lblNewLabel = new JLabel("Status:");
		panel.add(lblNewLabel);

		lblNewLabel_1 = new JLabel("Waiting");
		panel.add(lblNewLabel_1);
	}

	private void setStatus(String status) {
		lblNewLabel_1.setText(status);
	}

	public static void deleteFolderContents(File folder) {
		File[] files = folder.listFiles();
		if (files != null) {
			for (File f : files) {
				if (f.isDirectory()) {
					deleteFolderContents(f);
				} else {
					f.delete();
				}
			}
		}
	}

	private void loadFromFile(final File f) throws SQLException {
		setStatus("Loading icons...");
		selected = null;
		comboBox.setEnabled(false);
		if (showReplaced) {
			reloadMapping();
		}
		new Thread() {
			@Override
			public void run() {
				if (!tempDir.exists()) {
					tempDir.mkdir();
				}
				final List<Component> toAdd = new ArrayList<>();
				deleteFolderContents(tempDir);
				try {
					Files.copy(rh.toPath(), rh_cp.toPath());
					Files.copy(f.toPath(), target.toPath());
					Process p = Runtime.getRuntime().exec("ResHacker.exe -extract " + target.getName() + ", shell.rc, icongroup,,", null, tempDir);
					p.waitFor();
					List<String> data = Files.readAllLines(new File(tempDir.getAbsolutePath() + "/shell.rc").toPath(), Charset.defaultCharset());
					List<String> toData = new ArrayList<>();
					for (String s : data) {
						if (!s.isEmpty()) {
							toData.add(s);
						}
					}
					int total = toData.size();
					int c = 0;
					ResIcon prev = null;
					for (String s : toData) {
						String[] d = s.split(" ");
						String id = d[0];
						File file = new File(tempDir.getAbsolutePath() + "/" + d[2].replaceAll("\"", ""));
						if (file.exists()) {
							setStatus("Processing " + c + "/" + total);
							ResIcon icon = new ResIcon(IconManager.this, id, file, prev, keyAdapt);
							prev = icon;
							mapping.put(id, icon);
							toAdd.add(icon);
							if (showReplaced) {
								if (mapar.contains(id)) {
									icon.setReplaced(true);
								}
							}
						}
						c++;
					}
				} catch (Exception e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(null, "Extraction failed");
				}
				try {
					SwingUtilities.invokeAndWait(new Runnable() {

						@Override
						public void run() {
							for (Component c : toAdd) {
								realContent.add(c);
							}
							setStatus("Waiting");
							comboBox.setEnabled(true);
							realContent.revalidate();
							realContent.repaint();
						}

					});
				} catch (InvocationTargetException | InterruptedException e) {
					JOptionPane.showMessageDialog(null, "UI operation failed");
				}

			}
		}.start();
	}

	private void reloadMapping() throws SQLException {
		mapar = replacer.getMappings(originalRoot);
	}

	protected void updateIcons() throws SQLException {
		if (started) {
			realContent.removeAll();
			mapping.clear();
			revalidate();
			repaint();
			int index = comboBox.getSelectedIndex();
			if (index != -1) {
				String archive = comboBox.getSelectedItem().toString();
				originalRoot = archive;
				String file = root + "/" + archive;
				File f = new File(file);
				if (!f.exists()) {
					JOptionPane.showMessageDialog(null, "Selected file does not exist");
				} else {
					loadFromFile(f);
				}
			}
		}
	}

	private static final long serialVersionUID = -8237378324756029223L;

	public ResIcon getSelectedIcon() {
		return selected;
	}

	public String getOriginalRoot() {
		return originalRoot;
	}

	public void previous() {
		if (selected != null) {
			selected.prev();
		}

	}

	public void next() {
		if (selected != null) {
			selected.next();
		}
	}

}
