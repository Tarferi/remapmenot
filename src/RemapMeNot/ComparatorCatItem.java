package RemapMeNot;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.JPanel;

import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ComparatorCatItem extends JPanel {
	private static Image arrowImg;
	static {
		arrowImg = Toolkit.getDefaultToolkit().getImage(Window.class.getResource("/RemapMeNot/arrow.png"));
	}

	public ComparatorCatItem(final Comparator parent, final String fromFile, final String toFile, final String fromID, final String toID) {
		final Image fromImage = parent.getFrom(fromFile, fromID);
		final Image toImage = parent.getTo(toFile, toID);

		setLayout(new MigLayout("", "[100px:100:100px][grow][100px:100:100px][40]", "[100][][]"));

		JPanel panel = new JPanel() {
			private static final long serialVersionUID = 1685417673342990882L;

			@Override
			public void paint(Graphics g) {
				super.paint(g);
				g.drawImage(toImage, 0, 0, 100, 100, null);
			}
		};
		add(panel, "cell 0 0,grow");

		JPanel panel_1 = new JPanel() {
			private static final long serialVersionUID = 1685417673342990882L;

			@Override
			public void paint(Graphics g) {
				super.paint(g);
				g.drawImage(arrowImg, 0, 0, 40, getHeight(), null);
			}
		};
		add(panel_1, "cell 1 0,grow");

		JPanel panel_2 = new JPanel() {
			private static final long serialVersionUID = 1685417673342990882L;

			@Override
			public void paint(Graphics g) {
				super.paint(g);
				g.drawImage(fromImage, 0, 0, 100, 100, null);
			}
		};
		add(panel_2, "cell 2 0,grow");
		this.setSize(new Dimension(360, 150));
		this.setMinimumSize(new Dimension(360, 150));
		this.setPreferredSize(new Dimension(360, 150));
		this.setMaximumSize(new Dimension(360, 150));

		final JButton btnDelete = new JButton("DELETE");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				parent.deleteItemByData(fromFile, toFile, fromID, toID);
				btnDelete.setEnabled(false);
			}
		});
		add(btnDelete, "cell 3 0 1 3,growy");

		JLabel label = new JLabel(fromImage.getWidth(null) + ":" + fromImage.getHeight(null));
		add(label, "cell 2 1,alignx center");

		JLabel label_1 = new JLabel(toImage.getWidth(null) + ":" + toImage.getHeight(null));
		add(label_1, "cell 0 1,alignx center");

		JLabel lblShell = new JLabel(fromFile + ":" + fromID);
		add(lblShell, "cell 2 2");

		JLabel lblShelldll = new JLabel(toFile + ":" + toID);
		add(lblShelldll, "cell 0 2");

	}

	private static final long serialVersionUID = 8766185732619732755L;

}
