package RemapMeNot;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ReplacerDB {

	private Connection c;

	public ReplacerDB() throws Exception {
		c = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:target.db");
		} catch (Exception e) {
			throw new Exception();
		}
		init();
	}

	private void init() throws SQLException {
		for (String loc : Locations.Locations) {
			loc = fileToDBName(loc);
			Statement stmt;
			stmt = c.createStatement();
			String sql = "CREATE TABLE IF NOT EXISTS " + loc + " " + "(ID TEXT PRIMARY KEY     NOT NULL," + " FROMFILE         TEXT     NOT NULL, " + " FROMID           TEXT     NOT NULL)";
			stmt.executeUpdate(sql);
		}
	}

	private static String fileToDBName(String file) {
		return file.toLowerCase().replace("/", "�").replace(".", "�").replace(":", "�");
	}

	private static String DBNameToFile(String file) {
		return file.toLowerCase().replace("�", "/").replace("�", ".").replace("�", ":");
	}

	public void add(ResIcon fromI, ResIcon toI) throws SQLException {
		String from = fileToDBName(fromI.getSource().getOriginalRoot());
		String to = fileToDBName(toI.getSource().getOriginalRoot());
		String sql = "SELECT * FROM " + from + " WHERE ID='" + fromI.getID() + "'";
		Statement stmt = c.createStatement();
		ResultSet res = stmt.executeQuery(sql);
		if (res.next()) {
			sql = "UPDATE " + from + " SET FROMFILE='" + to + "', FROMID='" + toI.getID() + "' WHERE ID='" + fromI.getID() + "'";
		} else {
			sql = "INSERT INTO " + from + " (ID, FROMFILE, FROMID) VALUES ('" + fromI.getID() + "','" + to + "', '" + toI.getID() + "')";
		}
		res.close();
		stmt.executeUpdate(sql);
		stmt.close();
	}

	public void clear(ResIcon fromI) throws SQLException {
		String from = fileToDBName(fromI.getSource().getOriginalRoot());
		String sql = "SELECT * FROM " + from + " WHERE ID='" + fromI.getID() + "'";
		Statement stmt = c.createStatement();
		ResultSet res = stmt.executeQuery(sql);
		if (res.next()) {
			sql = "DELETE FROM " + from + " WHERE ID='" + fromI.getID() + "'";
			stmt.executeUpdate(sql);
			stmt.close();
		}
	}

	public List<ComparatorCat> getCats(Comparator comparator) throws SQLException {
		List<ComparatorCat> lst = new ArrayList<>();
		String sql = "SELECT name FROM sqlite_master WHERE type='table'";
		Statement stmt = c.createStatement();
		ResultSet res = stmt.executeQuery(sql);
		List<ComparatorCat> ar = new ArrayList<>();
		while (res.next()) {
			String toFile = DBNameToFile(res.getString(1));
			List<ComparatorCatItem> items = getCatItems(comparator, toFile);
			ComparatorCat cat = new ComparatorCat(toFile, items);
			ar.add(cat);
			comparator.addCat(cat);
		}
		return lst;
	}

	public List<ComparatorCatItem> getCatItems(Comparator comparator, String toFile) throws SQLException {
		List<ComparatorCatItem> lst = new ArrayList<>();
		String sourceFileEsc = fileToDBName(toFile);
		String sql = "SELECT * FROM " + sourceFileEsc;
		Statement stmt = c.createStatement();
		ResultSet res = stmt.executeQuery(sql);
		while (res.next()) {
			String toID = res.getString(1);
			String fromFile = DBNameToFile(res.getString(2));
			String fromID = res.getString(3);
			ComparatorCatItem item = new ComparatorCatItem(comparator, fromFile, toFile, fromID, toID);
			lst.add(item);
		}
		return lst;
	}

	public List<String> getFiles() throws SQLException {
		String sql = "SELECT name FROM sqlite_master WHERE type='table'";
		Statement stmt = c.createStatement();
		ResultSet res = stmt.executeQuery(sql);
		List<String> ar = new ArrayList<>();
		while (res.next()) {
			String db = DBNameToFile(res.getString(1));
			ar.add(db);
		}
		return ar;
	}

	public List<String[]> getFilesRemapping(String originalRoot) throws SQLException {
		String root = fileToDBName(originalRoot);
		String sql = "SELECT * FROM " + root;
		Statement stmt = c.createStatement();
		ResultSet res = stmt.executeQuery(sql);
		List<String[]> ar = new ArrayList<>();
		while (res.next()) {
			String toID = res.getString(1);
			String fromFile = DBNameToFile(res.getString(2));
			String fromID = res.getString(3);
			ar.add(new String[] { originalRoot, fromFile, toID, fromID });
		}
		return ar;
	}

	public List<String> getMappings(String originalRoot) throws SQLException {
		originalRoot = fileToDBName(originalRoot);
		String sql = "SELECT * FROM " + originalRoot;
		Statement stmt = c.createStatement();
		ResultSet res = stmt.executeQuery(sql);
		List<String> ar = new ArrayList<>();
		while (res.next()) {
			String id = res.getString(1);
			ar.add(id);
		}
		return ar;
	}

	public void removeByData(String fromFile, String toFile, String fromID, String toID) throws SQLException {
		Statement stmt = c.createStatement();
		String from = fileToDBName(toFile);
		String to = fileToDBName(fromFile);
		String sql = "DELETE FROM " + from + " WHERE ID='" + toID + "' AND FROMFILE='" + to + "' AND FROMID='" + fromID + "'";
		stmt.executeUpdate(sql);

	}
}
