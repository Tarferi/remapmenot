package RemapMeNot;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollBar;

import java.awt.BorderLayout;
import java.awt.Image;

import javax.swing.JSplitPane;

import java.awt.Toolkit;

import javax.swing.JPanel;
import javax.swing.JButton;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import java.sql.SQLException;

import net.miginfocom.swing.MigLayout;

import java.awt.Dimension;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.FlowLayout;
import java.io.IOException;

import javax.swing.JScrollPane;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

public class Window extends JFrame {

	private final String loc1;
	private final String loc2;
	private IconManager iconManager;
	private IconManager iconManager_1;
	private ReplacerDB replacer;
	private KeyListener keyAdapt = new KeyAdapter() {
		@Override
		public void keyPressed(KeyEvent e) {
			if (e.getKeyCode() == KeyEvent.VK_LEFT) {
				if (e.isControlDown()) {
					iconManager.previous();
				} else if (e.isAltDown() || e.isAltGraphDown()) {
					iconManager.previous();
					iconManager_1.previous();
				} else {
					iconManager_1.previous();
				}
			} else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
				if (e.isControlDown()) {
					iconManager.next();
				} else if (e.isAltDown() || e.isAltGraphDown()) {
					iconManager.next();
					iconManager_1.next();
				} else {
					iconManager_1.next();
				}
			} else if (e.getKeyCode() == KeyEvent.VK_SPACE) {
				addRequested();
			} else if (e.getKeyCode() == KeyEvent.VK_DELETE) {
				removeRequested();
			}
		}
	};

	public KeyListener getKeyAdapter() {
		return keyAdapt;
	}

	public void start() {
		iconManager.start();
		iconManager_1.start();
	}

	public Window(String locFrom, String locTo, final String lgcode) {
		this.loc1 = locFrom;
		this.loc2 = locTo;
		addKeyListener(keyAdapt);
		setSize(new Dimension(640, 480));
		try {
			replacer = new ReplacerDB();
		} catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Databaze creation failed");
		}
		setIconImage(Toolkit.getDefaultToolkit().getImage(Window.class.getResource("/RemapMeNot/daico-0.png")));
		setVisible(true);
		setTitle("RemapMeNot");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(new BorderLayout(0, 0));

		panel = new JPanel();
		getContentPane().add(panel, BorderLayout.NORTH);

		panel.setLayout(new MigLayout("", "[grow][][][grow]", "[]"));

		JButton ok = new JButton(getButton("ok"));
		ok.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				addRequested();
			}

		});

		panel.add(ok, "cell 1 0");

		JButton del = new JButton(getButton("del"));
		del.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				removeRequested();

			}

		});
		panel.add(del, "cell 2 0");

		panel_1 = new JPanel();
		getContentPane().add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(new BorderLayout(0, 0));

		JSplitPane splitPane = new JSplitPane();
		panel_1.add(splitPane, BorderLayout.CENTER);
		splitPane.setResizeWeight(0.5);
		splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);

		iconManager = new IconManager(loc1, false, replacer, keyAdapt);
		splitPane.setLeftComponent(iconManager);

		iconManager_1 = new IconManager(loc2, true, replacer, keyAdapt);
		splitPane.setRightComponent(iconManager_1);

		panel_2 = new JPanel();
		panel_2.setPreferredSize(new Dimension(300, 10));
		panel_1.add(panel_2, BorderLayout.EAST);
		panel_2.setLayout(new BorderLayout(0, 0));

		scrollPane = new JScrollPane();
		scrollPane.getVerticalScrollBar().setUnitIncrement(16);
		panel_2.add(scrollPane);

		panel_3 = new JPanel();
		scrollPane.setViewportView(panel_3);
		panel_3.setLayout(new WrapLayout(FlowLayout.CENTER, 5, 5));

		menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		mnDatabase = new JMenu("Database");
		menuBar.add(mnDatabase);

		mntmViewComparision = new JMenuItem("View comparision");
		mntmViewComparision.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new Comparator(loc1, loc2, replacer);
			}
		});
		mnDatabase.add(mntmViewComparision);

		mntmGenerateScripts = new JMenuItem("Generate scripts");
		mntmGenerateScripts.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Generator(loc1, loc2, replacer, lgcode);
			}
		});
		mnDatabase.add(mntmGenerateScripts);

		this.pack();
		setSize(953, 501);
	}

	public void removeRequested() {
		ResIcon sel = iconManager_1.getSelectedIcon();
		if (sel != null) {
			try {
				replacer.clear(sel);
				sel.setReplaced(false);
			} catch (SQLException e) {
				e.printStackTrace();
				JOptionPane.showMessageDialog(null, "Operation failed");
			}
		}
	}

	public void addRequested() {
		ResIcon sel1 = iconManager.getSelectedIcon();
		ResIcon sel2 = iconManager_1.getSelectedIcon();
		if (sel1 != null && sel2 != null) {
			try {
				replacer.add(sel2, sel1);
				sel2.setReplaced(true);
				RemapLog log = new RemapLog(sel2, sel1);
				addLog(log);
			} catch (SQLException | IOException e) {
				e.printStackTrace();
				JOptionPane.showMessageDialog(null, "Operation failed");
			}
		}
	}

	private void scrollToBottom() {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				JScrollBar vertical = scrollPane.getVerticalScrollBar();
				vertical.setValue(vertical.getMaximum());
			}
		});
	}

	public void addLog(RemapLog log) {
		panel_3.add(log);
		panel_3.revalidate();
		panel_3.repaint();
		scrollToBottom();
	}

	private ImageIcon getButton(String image) {
		Image arrow = Toolkit.getDefaultToolkit().getImage(Window.class.getResource("/RemapMeNot/" + image + ".png"));
		return new ImageIcon(arrow);
	}

	private static final long serialVersionUID = -8255319694373975038L;
	private JPanel panel;
	private JPanel panel_1;
	private JPanel panel_2;
	private JScrollPane scrollPane;
	private JPanel panel_3;
	private JMenuBar menuBar;
	private JMenu mnDatabase;
	private JMenuItem mntmViewComparision;
	private JMenuItem mntmGenerateScripts;

}
