package RemapMeNot;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import java.awt.Toolkit;

import net.miginfocom.swing.MigLayout;

import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileFilter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Generator extends JFrame {

	private JLabel lblFile;
	private JLabel lblProgress;
	private JLabel lblTotal;
	private Thread thr;
	private String lgCode;

	public Generator(final String loc1, final String loc2, final ReplacerDB replacer, String lgcode) {
		this.lgCode = lgcode;
		setType(Type.UTILITY);
		setResizable(false);
		addWindowListener(new WindowAdapter() {
			@SuppressWarnings("deprecation")
			@Override
			public void windowClosing(WindowEvent arg0) {
				if (thr != null) {
					try {
						thr.stop();
					} catch (Throwable e) {
					}
				}
			}
		});
		setTitle("RMN Generator");
		setIconImage(Toolkit.getDefaultToolkit().getImage(Generator.class.getResource("/RemapMeNot/daico-0.png")));
		getContentPane().setLayout(new MigLayout("", "[][]", "[][][]"));

		JLabel lblCurrentlyProcessingFile = new JLabel("Currently processing file:");
		lblCurrentlyProcessingFile.setFont(new Font("Tahoma", Font.BOLD, 11));
		getContentPane().add(lblCurrentlyProcessingFile, "cell 0 0,alignx right");

		lblFile = new JLabel("");
		getContentPane().add(lblFile, "cell 1 0");

		JLabel lblProgresssss = new JLabel("Progress:");
		lblProgresssss.setFont(new Font("Tahoma", Font.BOLD, 11));
		getContentPane().add(lblProgresssss, "cell 0 1,alignx right");

		lblProgress = new JLabel("");
		getContentPane().add(lblProgress, "cell 1 1");

		JLabel lblTotalPrsssogress = new JLabel("Total progress:");
		lblTotalPrsssogress.setFont(new Font("Tahoma", Font.BOLD, 11));
		getContentPane().add(lblTotalPrsssogress, "cell 0 2,alignx right");

		lblTotal = new JLabel("Loading");
		getContentPane().add(lblTotal, "cell 1 2");
		pack();
		setVisible(true);
		setSize(430, 95);
		thr = new Thread() {
			@Override
			public void run() {
				try {
					List<String> tables = replacer.getFiles();
					List<List<String[]>> qdata = new ArrayList<>();
					lblTotal.setText("Loading files list");
					deleteDir("generator_target");
					deleteDir("generator_result");
					for (String s : tables) {
						List<String[]> datta = replacer.getFilesRemapping(s);
						if (!datta.isEmpty()) {
							qdata.add(datta);
						}
					}
					lblTotal.setText("Preparing directories files");
					for (List<String[]> data : qdata) {
						unExtractAll(data, loc1);
					}
					lblTotal.setText("Extracting files");
					for (List<String[]> data : qdata) {
						extractAll(data, loc1);
					}
					lblTotal.setText("Extracting files");
					for (List<String[]> data : qdata) {
						copySourceFilesAll(data, loc1, loc2);
					}
					/*
					 * lblTotal.setText("Compiling"); for (List<String[]> data :
					 * qdata) { compileAll(data, loc1); }
					 * lblTotal.setText("Building"); for (List<String[]> data :
					 * qdata) { buildAll(data, loc2); }
					 */
					lblTotal.setText("Cleaning up");
					for (List<String[]> data : qdata) {
						cleanAll(data, loc1);
					}
					lblFile.setText("");
					lblProgress.setText("Finished");
					lblTotal.setText("Finished");
				} catch (SQLException e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(null, "Processing table list failed");
					return;
				}
			}
		};
		thr.start();
	}

	private boolean delete(String file) {
		return delete(new File(file));
	}

	private boolean delete(File file) {
		if (file.exists()) {
			if (file.isDirectory()) {
				File[] files = file.listFiles();
				for (File f : files) {
					delete(f);
				}
			} else {
				file.delete();
			}
		}
		return true;
	}

	private boolean execute(String command, String path) {
		return execute(command, new File(path));
	}

	private boolean execute(String command, File path) {
		Process p;
		try {
			p = Runtime.getRuntime().exec(command, null, path);
			p.waitFor();
			return true;
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Executing \"" + command + "\" failed");
			return false;
		}
	}

	private boolean copy(String from, String to) {
		return copy(from, to, false);
	}

	private boolean copy(File from, File to, boolean overwrite) {
		if (from.exists()) {
			if (to.exists()) {
				if (overwrite) {
					to.delete();
				} else {
					return true;
				}
			}
			try {
				Files.copy(from.toPath(), to.toPath());
				return true;
			} catch (IOException e2) {
				e2.printStackTrace();
				JOptionPane.showMessageDialog(null, "Failed to copy files");
			}
		}
		return false;
	}

	private boolean copy(String from, String to, boolean overwrite) {
		File f = new File(from);
		File t = new File(to);
		return copy(f, t, overwrite);
	}

	private boolean writeFile(String file, String data, boolean overwrite) {
		return writeFile(new File(file), data, overwrite);
	}

	private boolean writeFile(File file, String data, boolean overwrite) {
		FileOutputStream fs = null;
		try {
			fs = new FileOutputStream(file);
			fs.write(data.getBytes());
			try {
				fs.close();
			} catch (IOException e) {
			}
			return true;
		} catch (Throwable e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Failed to write file \"" + file.getPath() + "\"");
			try {
				fs.close();
			} catch (IOException ee) {
			}
			return false;
		}
	}

	private void buildAll(List<String[]> data, String loc2) {
		lblTotal.setText("Building new files");
		createDirIfNotExists("generator");
		createDirIfNotExists("generator_result");
		createDirIfNotExists("generator_result/new");
		if (data.size() > 0) {
			String gname = new File(data.get(0)[0]).getName();
			String fa = data.get(0)[0];
			File orig = new File(loc2 + "/" + fa);
			if (!orig.exists()) {
				JOptionPane.showMessageDialog(null, "File \"" + fa + "\" does not exist");
				return;
			}
			if (writeFile("generator_result/build.bat", "ResHack.exe -addoverwrite " + gname + ", new/" + gname + ", " + gname + ".res,ICO,," + lgCode, true)) {
				if (copy("ResHacker2.exe", "generator_result/ResHack.exe")) {
					if (delete("generator_result/" + gname)) {
						if (copy(orig.getAbsolutePath(), "generator_result/" + gname)) {
							if (execute("cmd /c build.bat", "generator_result")) {
								if (delete("generator_result/build.bat")) {
									if (delete("generator_result/" + gname)) {
										if (delete("generator_result/ResHack.exe")) {
											return;
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	private void cleanAll(List<String[]> data, String loc1) {
		unExtractAll(data, loc1);
		deleteDir("generator");
		deleteDir("generator_target");
	}

	private void compileAll(List<String[]> data, String root) {
		for (String[] str : data) {
			String toFile = str[0];
			compileFile(toFile);
		}
	}

	private void compileFile(String toFile) {
		lblFile.setText(toFile);
		lblProgress.setText("Compiling");
		createDirIfNotExists("generator");
		createDirIfNotExists("generator_target");
		createDirIfNotExists("generator_result");

		File ff = new File(toFile);
		String targetDir = "generator_target/" + ff.getName();

		File res = new File(targetDir + "/shell.res");
		File shellrc = new File(targetDir + "/shell.rc");
		if (!res.exists()) {
			if (shellrc.exists()) {
				if (writeFile(targetDir + "/compile.bat", "ResHacker.exe -compile shell.rc,shell.res", true)) {
					if (execute("cmd /c compile.bat", targetDir)) {
						if (!res.exists()) {
							JOptionPane.showMessageDialog(null, "Compilation failed");
						} else {
							lblProgress.setText("Copying");
							if (copy(res.getAbsolutePath(), "generator_result/" + ff.getName() + ".res")) {
								return;
							}
						}
					}
				}

			}
		}
	}

	private void copySourceFilesAll(List<String[]> data, String root, String targetRoot) {
		lblTotal.setText("Copying result files");
		StringBuilder sb = new StringBuilder();
		sb.append("LANGUAGE " + lgCode + ", 0\r\n\r\n");
		StringBuilder scr = new StringBuilder();
		scr.append("[FILENAMES]\r\n");
		scr.append("Exe=source.dll\r\n");
		scr.append("Saveas=target.dll\r\n\r\n");
		scr.append("[COMMANDS]\r\n");
		int index = 0;
		for (String[] str : data) {
			String toFile = str[0];
			String fromFile = str[1];
			String toID = str[2];
			String fromID = str[3];
			copySourceFile(fromFile, toFile, fromID, toID, root, index, sb, scr);
			index++;
		}
		String path1 = data.get(0)[0];
		String fromPath = data.get(0)[1];
		String fileName = new File(path1).getName();
		File file = new File(targetRoot + "/" + data.get(0)[0]);
		File sourceFile = new File(root + "/" + data.get(0)[1]);
		if (!file.exists()) {
			JOptionPane.showMessageDialog(null, "Source file \"" + fileName + "\" does not exist");
		} else {
			String dir = "generator_target/" + fileName;
			if (data.size() > 0) {
				if (writeFile(dir + "/shell.rc", sb.toString(), true)) {
					if (writeFile(dir + "/scr.scr", scr.toString(), true)) {
						if (copy(file.getAbsolutePath(), dir + "/source.dll", true)) {
							if (copy("ResHacker2.exe", dir + "/ResHacker.exe", true)) {
								if (writeFile(dir + "/build.bat", "ResHacker.exe -script scr.scr", true)) {
									lblProgress.setText("Precompilation");
									if (execute("cmd /c build.bat", dir)) {
										File target = new File(dir + "/target.dll");
										if (!target.exists()) {
											JOptionPane.showMessageDialog(null, "Precompilation of \"" + fileName + "\" failed");
										} else {
											createDirIfNotExists("generator_result");
											createDirIfNotExists("generator_result/bin");
											if (copy(target.getAbsolutePath(), "generator_result/bin/" + fileName, true)) {
												String frm = new File(fromPath).getPath();
												createDirIfNotExists("generator_result/src/" + frm);
												if (copy(sourceFile.getAbsolutePath(), "generator_result/src/" + fromPath, true)) {
													return;
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	private static final void createDirIfNotExists(String dir) {
		File rt = new File(dir);
		if (!rt.exists()) {
			if (rt.getParentFile() != null) {
				if (!rt.getParentFile().exists()) {
					createDirIfNotExists(rt.getParentFile().getAbsolutePath());
				}
			}
			rt.mkdir();
		}
	}

	private static final void deleteDir(String dir) {
		File rt = new File(dir);
		if (rt.exists()) {
			if (rt.isDirectory()) {
				File[] list = rt.listFiles((FileFilter) null);
				for (File f : list) {
					if (f.isDirectory()) {
						deleteDir(f.getAbsolutePath());
					} else {
						f.delete();
					}
				}
			}
			rt.delete();
		}
	}

	private void copySourceFile(String fromFile, String toFile, String fromID, String toID, String root, int nextAvailableID, StringBuilder sb, StringBuilder other) {
		lblFile.setText(toFile);
		lblProgress.setText("Searching resource");
		File ff = new File(fromFile);
		File fff = new File(toFile);
		createDirIfNotExists("generator");
		createDirIfNotExists("generator_target");
		String targetDir = "generator/" + ff.getName();
		String mainTargetDir = "generator_target/" + fff.getName();
		createDirIfNotExists(mainTargetDir);
		File shellrc = new File(targetDir + "/shell.rc");
		if (!shellrc.exists()) {
			JOptionPane.showMessageDialog(null, "Shell.rc does not exist");
			return;
		} else {
			if (copy("ResHacker2.exe", mainTargetDir + "/ResHacker.exe", false)) {
				List<String> data;
				try {
					data = Files.readAllLines(shellrc.toPath(), Charset.defaultCharset());
				} catch (IOException e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(null, "Reading shell.rc failed");
					return;
				}
				for (String s : data) {
					if (!s.isEmpty()) {
						String[] q = s.split(" ");
						String nid = q[0];
						if (nid.toLowerCase().equals(fromID.toLowerCase())) {
							lblProgress.setText("Processing resource");
							File imgFile = new File(shellrc.getParentFile().getAbsolutePath() + "/" + q[2].replaceAll("\"", ""));
							String icon = "icon_" + nextAvailableID + ".ico";
							String Icn = mainTargetDir + "/" + icon;
							File nwIcon = new File(Icn);
							if (copy(imgFile, nwIcon, false)) {
								sb.append(toID + " ICON \"" + icon + "\"\r\n\r\n");
								other.append("-addoverwrite " + nwIcon.getName() + " ,ICONGROUP," + toID + "," + lgCode + "\r\n");
								return;
							}
						}
					}
				}
				JOptionPane.showMessageDialog(null, "Resource not found");
				return;
			}
		}

	}

	private void extractAll(List<String[]> files, String root) {
		lblTotal.setText("Extracting");
		for (String[] str : files) {
			String fromFile = str[1];
			extract(fromFile, root);
		}
	}

	private void unExtractAll(List<String[]> files, String root) {
		lblTotal.setText("Extracting");
		for (String[] str : files) {
			String fromFile = str[1];
			unExtract(fromFile, root);
		}
	}

	private void unExtract(String file, String root) {
		lblFile.setText(file);
		lblProgress.setText("Checking directory");
		File ff = new File(file);
		createDirIfNotExists("generator");
		String targetDir = "generator/" + ff.getName();
		deleteDir(targetDir);
	}

	private void extract(String file, String root) {
		lblFile.setText(file);
		File ff = new File(file);
		createDirIfNotExists("generator");
		String targetDir = "generator/" + ff.getName();
		createDirIfNotExists(targetDir);
		File shellrc = new File(targetDir + "/shell.rc");
		if (!shellrc.exists()) {
			lblProgress.setText("Extracting");
			File f = new File(root + "/" + file);
			if (!f.exists()) {
				JOptionPane.showMessageDialog(null, "File does not exist");
			} else {
				File target = new File(targetDir + "/" + f.getName());
				if (delete(target)) {
					if (copy(f.getAbsolutePath(), target.getAbsolutePath())) {
						if (delete(targetDir + "/ResHacker.exe")) {
							if (copy("ResHacker.exe", targetDir + "/ResHacker.exe")) {
								if (execute("ResHacker.exe -extract " + target.getName() + ", shell.rc, icongroup,,", targetDir)) {
									if (!shellrc.exists()) {
										JOptionPane.showMessageDialog(null, "Shell was not created");
										return;
									} else {
										return;
									}
								}
							}
						}
					}
				}
			}
		}
	}

	private static final long serialVersionUID = -5831648669613711565L;

}
