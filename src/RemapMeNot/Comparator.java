package RemapMeNot;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import net.sf.image4j.codec.ico.ICODecoder;

import java.awt.BorderLayout;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JScrollPane;
import javax.swing.JPanel;

import net.miginfocom.swing.MigLayout;

import javax.swing.BoxLayout;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.Toolkit;

public class Comparator extends JFrame {

	private Map<String, Image> fromMap = new HashMap<>();
	private Map<String, Image> toMap = new HashMap<>();
	private String fromRoot;
	private String toRoot;
	private JLabel lblFile;
	private JPanel panel_1;
	private Thread thr;
	private ReplacerDB replacer;

	public Comparator(String fromRoot, String toRoot, final ReplacerDB replacer) {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Comparator.class.getResource("/RemapMeNot/daico-0.png")));
		setTitle("RMN Comparator");
		this.replacer = replacer;
		addWindowListener(new WindowAdapter() {
			@SuppressWarnings("deprecation")
			@Override
			public void windowClosing(WindowEvent arg0) {
				if (thr != null) {
					try {
						thr.stop();
					} catch (Throwable e) {
					}
				}
			}
		});
		setResizable(false);
		this.fromRoot = fromRoot;
		this.toRoot = toRoot;
		getContentPane().setLayout(new BorderLayout(0, 0));

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.getVerticalScrollBar().setUnitIncrement(16);
		getContentPane().add(scrollPane, BorderLayout.CENTER);

		panel_1 = new JPanel();
		scrollPane.setViewportView(panel_1);
		panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.Y_AXIS));

		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.NORTH);
		panel.setLayout(new MigLayout("", "[][][][][]", "[]"));

		JLabel lblCurrentlyProcessingFile = new JLabel("Currently processing file:");
		panel.add(lblCurrentlyProcessingFile, "cell 0 0");

		lblFile = new JLabel("<FILE>");
		panel.add(lblFile, "cell 1 0");

		JLabel lblStatus = new JLabel("Status:");
		panel.add(lblStatus, "cell 3 0");

		statusCount = new JLabel("<STATUS>");
		panel.add(statusCount, "cell 4 0");
		pack();
		setSize(390, 800);
		setVisible(true);
		thr = new Thread() {
			@Override
			public void run() {
				try {
					replacer.getCats(Comparator.this);
					lblFile.setText("");
					statusCount.setText("");
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		};
		thr.start();
	}

	public void addCat(final ComparatorCat cat) {
		try {
			SwingUtilities.invokeAndWait(new Runnable() {

				@Override
				public void run() {
					panel_1.add(cat);
				}

			});
		} catch (InvocationTargetException | InterruptedException e) {
			e.printStackTrace();
		}
	}

	private static final void createDirIfNotExists(String dir) {
		File rt = new File(dir);
		if (!rt.exists()) {
			rt.mkdir();
		}
	}

	private static final long serialVersionUID = -7471961923048865478L;
	private static final Image emptyImage = new BufferedImage(10, 10, 10);

	private final Image loadImage(Map<String, Image> saver, String file, String id, File shellrc) {
		try {
			List<String> data = Files.readAllLines(shellrc.toPath(), Charset.defaultCharset());

			for (String s : data) {
				if (!s.isEmpty()) {
					String[] q = s.split(" ");
					String nid = q[0];
					if (nid.trim().toLowerCase().equals(id)) {
						File imgFile = new File(shellrc.getParentFile().getAbsolutePath() + "/" + q[2].replaceAll("\"", ""));
						List<BufferedImage> images;
						try {
							images = ICODecoder.read(imgFile);
						} catch (IOException e) {
							e.printStackTrace();
							JOptionPane.showMessageDialog(null, "Extraction failed");
							return emptyImage;
						}
						int maxW = 0;
						int maxH = 0;
						Image icon;
						if (images.isEmpty()) {
							icon = new BufferedImage(10, 10, 10);
							System.out.println("No image found for " + file + ":" + id);
						} else {
							Image maxImg = images.get(0);
							for (Image img : images) {
								int nw = img.getWidth(null);
								if (nw > maxW) {
									int nh = img.getHeight(null);
									if (nh > maxH) {
										maxImg = img;
										maxW = nw;
										maxH = nh;
									}
								}
							}
							icon = maxImg;
						}
						fromMap.get(file.toLowerCase() + ":" + id.toLowerCase());
						return icon;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Extraction failed");
		}
		System.out.println("No image found for " + file + ":" + id);
		return emptyImage;
	}

	private final Image tryExtract(Map<String, Image> saver, String file, String id, String fromto, String root) {
		lblFile.setText(file);
		statusCount.setText("Extracting");
		createDirIfNotExists("comparator");
		createDirIfNotExists("comparator/" + fromto);
		File ff = new File(file);
		String targetDir = "comparator/" + fromto + "/" + ff.getName();

		File shellrc = new File(targetDir + "/shell.rc");
		if (shellrc.exists()) {
			return loadImage(saver, file, id, shellrc);
		}
		createDirIfNotExists(targetDir);

		File f = new File(root + "/" + file);
		if (!f.exists()) {
			JOptionPane.showMessageDialog(null, "File does not exist");
			return emptyImage;
		}
		File target = new File(targetDir + "/" + f.getName());
		if (!target.exists()) {
			try {
				Files.copy(f.toPath(), target.toPath());
			} catch (IOException e) {
				e.printStackTrace();
				JOptionPane.showMessageDialog(null, "File copy failed");
				return emptyImage;
			}
		}
		File rh = new File("ResHacker.exe");
		File rh_cp = new File(targetDir + "/ResHacker.exe");
		if (!rh_cp.exists()) {
			try {
				Files.copy(rh.toPath(), rh_cp.toPath());
			} catch (IOException e1) {
				e1.printStackTrace();
				JOptionPane.showMessageDialog(null, "RH file copy failed");
				return emptyImage;
			}
		}
		Process p;
		File tempDir = new File(targetDir);
		try {
			p = Runtime.getRuntime().exec("ResHacker.exe -extract " + target.getName() + ", shell.rc, icongroup,,", null, tempDir);
			p.waitFor();
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Extraction failed");
			return emptyImage;
		}

		if (shellrc.exists()) {
			return loadImage(saver, file, id, shellrc);
		} else {
			JOptionPane.showMessageDialog(null, "Shell was not created");
			return emptyImage;
		}
	}

	private JLabel statusCount;

	public Image getFrom(String fromFile, String fromID) {
		if (fromMap.containsKey(fromFile.toLowerCase() + ":" + fromID.toLowerCase())) {
			return fromMap.get(fromFile.toLowerCase() + ":" + fromID.toLowerCase());
		}
		return tryExtract(fromMap, fromFile, fromID, "from", fromRoot);
	}

	public Image getTo(String toFile, String toID) {
		if (toMap.containsKey(toFile + ":" + toID)) {
			return toMap.get(toFile + ":" + toID);
		}
		return tryExtract(toMap, toFile, toID, "to", toRoot);
	}

	public void deleteItemByData(String fromFile, String toFile, String fromID, String toID) {
		try {
			replacer.removeByData(fromFile, toFile, fromID, toID);
		} catch (SQLException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Delete operation failed");
		}
	}


}
