package RemapMeNot;

import javax.swing.JPanel;

import java.awt.BorderLayout;

import javax.swing.JLabel;

import java.awt.Font;

import net.miginfocom.swing.MigLayout;

import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ComparatorCat extends JPanel {
	private static final long serialVersionUID = 6987763797981452960L;

	private boolean hidden = false;

	private JPanel panel_1;

	private JButton btnHide;

	public void setHidden(boolean hid) {
		hidden = hid;
		if (hidden) {
			btnHide.setText("Show");
			panel_1.setVisible(false);
		} else {
			btnHide.setText("Hide");
			panel_1.setVisible(true);
		}
	}
	
	public ComparatorCat(String root, List<ComparatorCatItem> items) {

		setLayout(new BorderLayout(0, 0));

		JPanel panel = new JPanel();
		add(panel, BorderLayout.NORTH);
		panel.setLayout(new MigLayout("", "[][][grow][]", "[]"));

		JLabel lblLocation = new JLabel("Location:");
		panel.add(lblLocation, "cell 0 0");
		lblLocation.setFont(new Font("Tahoma", Font.BOLD, 13));

		JLabel label = new JLabel(root);
		panel.add(label, "cell 1 0");

		btnHide = new JButton("Hide");
		btnHide.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setHidden(!hidden);
			}
		});
		panel.add(btnHide, "cell 3 0");

		panel_1 = new JPanel();
		add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.Y_AXIS));
		for (ComparatorCatItem item : items) {
			panel_1.add(item);
		}
		setHidden(true);
	}

}
