package RemapMeNot;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.swing.JPanel;

import net.sf.image4j.codec.ico.ICODecoder;

import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

public class ResIcon extends JPanel {

	private static final long serialVersionUID = -7301216649909094156L;

	private final Image icon;

	private IconManager source;

	private final String id;

	private boolean selected;
	private boolean replaced = false;

	private static final int ICONSIZE = 80;
	private static final Color sel = new Color(0, 0, 255, 127);
	private static final Color rep = new Color(0, 255, 0, 127);

	public IconManager getSource() {
		return source;
	}

	public Image getImage() {
		return icon;
	}

	public void setSelected(boolean sel) {
		this.selected = sel;
		this.revalidate();
		this.repaint();
	}

	public void setReplaced() {
		setReplaced(true);
	}

	public void setReplaced(boolean replace) {
		this.replaced = replace;
		revalidate();
		repaint();
	}

	private ResIcon next = null;

	private ResIcon previous;

	public void setNext(ResIcon next) {
		this.next = next;
	}

	public void next() {
		if (next != null) {
			next.requestFocus();
		}
	}

	public void prev() {
		if (previous != null) {
			previous.requestFocus();
		}
	}

	public ResIcon(final IconManager source, String sourceID, File filePath, ResIcon previous, KeyListener keyAdapt) throws IOException {
		if (previous != null) {
			previous.setNext(this);
		}
		this.previous = previous;
		addKeyListener(keyAdapt);
		addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				source.setSelectedIcon(ResIcon.this);
			}
		});
		id = sourceID;
		this.source = source;
		addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {
				source.setSelectedIcon(ResIcon.this);
				requestFocus();
			}
		});
		List<BufferedImage> images = ICODecoder.read(filePath);
		int maxW = 0;
		int maxH = 0;
		if (images.isEmpty()) {
			icon = new BufferedImage(10, 10, 10);
			return;
		}
		Image maxImg = images.get(0);
		for (Image img : images) {
			int nw = img.getWidth(null);
			if (nw > maxW) {
				int nh = img.getHeight(null);
				if (nh > maxH) {
					maxImg = img;
					maxW = nw;
					maxH = nh;
				}
			}
		}
		icon = maxImg;
		Dimension size = new Dimension(ICONSIZE, ICONSIZE);
		this.setSize(size);
		this.setMinimumSize(size);
		this.setPreferredSize(size);
		this.setMaximumSize(size);
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		g.drawImage(icon, 0, 0, ICONSIZE, ICONSIZE, null);
		if (selected) {
			g.setColor(sel);
			g.fillRect(0, 0, ICONSIZE, ICONSIZE);
		}
		if (replaced) {
			g.setColor(rep);
			g.fillRect(0, 0, ICONSIZE, ICONSIZE);
		}
		Color c = new Color(0);
		g.setColor(c);
		g.drawString(this.id, 5, 10);
	}

	public String getID() {
		return id;
	}
}
