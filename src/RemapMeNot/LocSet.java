package RemapMeNot;

import java.awt.Dimension;
import java.awt.Toolkit;

import net.miginfocom.swing.MigLayout;

import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JPanel;

import java.awt.FlowLayout;

import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class LocSet extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField txtSrc;
	private JTextField txtDst;
	private JTextField txtLg;

	public LocSet(String locFr, String locT, String lgCode) {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				System.exit(0);
			}
		});
		setResizable(false);
		setTitle("RMN Settings");
		setIconImage(Toolkit.getDefaultToolkit().getImage(LocSet.class.getResource("/RemapMeNot/daico-0.png")));
		getContentPane().setLayout(new MigLayout("", "[][grow]", "[][][][grow]"));

		JLabel lblSourceSystem = new JLabel("Source system:");
		lblSourceSystem.setFont(new Font("Tahoma", Font.BOLD, 11));
		getContentPane().add(lblSourceSystem, "cell 0 0,alignx trailing");

		txtSrc = new JTextField(locFr);
		getContentPane().add(txtSrc, "cell 1 0,growx");
		txtSrc.setColumns(10);

		JLabel lblTargetSystem = new JLabel("Target system:");
		lblTargetSystem.setFont(new Font("Tahoma", Font.BOLD, 11));
		getContentPane().add(lblTargetSystem, "cell 0 1,alignx trailing");

		txtDst = new JTextField(locT);
		getContentPane().add(txtDst, "cell 1 1,growx");
		txtDst.setColumns(10);

		JLabel lblLanguageCode = new JLabel("Language code:");
		lblLanguageCode.setFont(new Font("Tahoma", Font.BOLD, 11));
		getContentPane().add(lblLanguageCode, "cell 0 2,alignx trailing");

		txtLg = new JTextField(lgCode);
		getContentPane().add(txtLg, "cell 1 2,growx");
		txtLg.setColumns(10);

		JPanel panel = new JPanel();
		getContentPane().add(panel, "cell 0 3 2 1,grow");
		panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		JButton btnLaunch = new JButton("Launch");
		btnLaunch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				runner(txtSrc.getText(), txtDst.getText(), txtLg.getText());
			}
		});
		panel.add(btnLaunch);

		JButton btnExit = new JButton("Exit");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		panel.add(btnExit);
		pack();
		Dimension dim=new Dimension(400,150);
		setSize(dim);
		setPreferredSize(dim);
		setVisible(true);
	}

	protected void runner(String from, String to, String lg) {
		Window w = new Window(from, to, lg);
		w.start();
		dispose();
	}

}
