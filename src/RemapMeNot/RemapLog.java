package RemapMeNot;

import javax.swing.JPanel;

import net.miginfocom.swing.MigLayout;

import java.io.IOException;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;

public class RemapLog extends JPanel {
	private Image img;

	public RemapLog(ResIcon from, ResIcon to) throws IOException {
		setLayout(new MigLayout("", "[100,grow][80,grow][100,grow]", "[100,grow]"));

		img = Toolkit.getDefaultToolkit().getImage(Window.class.getResource("/RemapMeNot/arrow.png"));
		final Image img1=from.getImage();
		JPanel panel_1 = new JPanel() {
			private static final long serialVersionUID = 3409812573152972611L;

			@Override
			public void paint(Graphics g) {
				super.paint(g);
				g.drawImage(img1, 0, 0, getWidth(), getHeight(), null);
			}
		};
		add(panel_1, "cell 0 0,grow");

		JPanel panel = new JPanel() {
			private static final long serialVersionUID = 3409812573152972611L;

			@Override
			public void paint(Graphics g) {
				super.paint(g);
				g.drawImage(img, 0, 0, getWidth(), getHeight(), null);
			}
		};
		add(panel, "cell 1 0,grow");
		Dimension dim = new Dimension(250, 100);
		this.setSize(dim);
		this.setMinimumSize(dim);
		this.setPreferredSize(dim);
		this.setMaximumSize(dim);

		final Image img2=to.getImage();
		JPanel panel_2 = new JPanel() {

			private static final long serialVersionUID = 5427531690168980455L;

			@Override
			public void paint(Graphics g) {
				super.paint(g);
				g.drawImage(img2, 0, 0, getWidth(), getHeight(), null);
			}
		};
		
		add(panel_2, "cell 2 0,grow");
	}

	private static final long serialVersionUID = 3414821550008737727L;

}
